import { Prisma } from 'prisma-binding';

const prisma = new Prisma({
  typeDefs: 'server/generated/prisma.graphql',
  endpoint: 'http://localhost:4466',
});

// const movies = prisma.query.movies(null, '{ id, title, year, rating, actors { name, birthday, country, directors { name }  } }')
//   .then((data) => {
//     console.log(JSON.stringify(data, null, 2));
//   });

const createMovie = async (movie) => {
  const createdMovie = await prisma.mutation.createMovie({
    data: { ...movie }
  }, '{ id, title, year, rating }');

  return createdMovie;
};

createMovie({
  title: 'Good guys',
  year: 2015,
  rating: 7.3
});
